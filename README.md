# R2C2 APIs

This repository contains a collection of documents that details how various APIs of the R2C2 Platform can be interacted with.

## Introduction

R2C2 APIs are accessed through HTTP requests, Socket.IO and WebSocket connections.

## Documentation format

Expected schema of each topic / request is documented as such:

![](./topic-document-explainer.jpg)

See [Sending HTTP requests](./sending-http-requests.md) for more details
### Socket.IO connection

[Socket.IO](https://socket.io) used for long-lived and real-time communication e.g. sending robot movement commands, motor status of a robot, etc

Different types of information is grouped via topics. Events in Socket.IO are used for this puprpose.

Data are encoded as JSON strings before being sent. However this is typically handled by the Socket.IO library. It often offers tools to convert / automatically convert the JSON strings to native types depending on the language / platform.

For example, movement commands would be sent via the `/perform/movement` topic as Socket.IO event in this Javascript example:

```javascript
const data = {x: 1, y: 0, z: 0, yaw: 0}
socket.emit("/perform/movement", data)
```

In another example, to receive motor status of a robot:

```javascript
socket.emit("/state/motor", (data) => {
    console.log("Motor state:", data) // data is typically of javascript types and plain objects.
})
```

Although Socket.IO supports request-response model through [acknowledgements](https://socket.io/docs/v4/emitting-events/#acknowledgements), we use HTTP for this purpose instead. See the [HTTP](#http) section.

### HTTP

These are used for interactions that requires a client-server model e.g. obtaining a list of mission, fetching a map, etc

Sections that are marked with `(HTTP)` are expected to be interacted with HTTP requests. Topic and expected data of the topic is sent to a specific endpoint.

See [Sending HTTP Requests](./sending-http-requests.md) for way to make the requests.

### WebSocket connection

Currently it is primarily used in video streaming. R2C2 uses a packet- and offset-based protocol for such purpose. 
See the streaming API docs <small>\[WIP, unavailable\]</small> for details.
<!-- See the [streaming API docs](./streaming.md) for details -->

## Table of contents

### Guides

- [Sending HTTP requests](./sending-http-requests.md)

### APIs
- [Robot Teleoperation](./robot_teleoperation.md)
- [Navigation](./navigation.md)
- Streaming <small>\[WIP, unavailable\]</small>

## Contributing

### Guidelines
There are several rules to adhere to when writing this documentation.

1. Be concise

    Put just enough context in explanation. Make sure information is correct.

2. Use English

    So everyone understands.

3. Be grammatically correct.
   
    To avoid misunderstandings. Writers are encouraged to use tools such as [Grammarly](https://www.grammarly.com), [Language Tool](https://languagetool.org) or [Vale](https://vale.sh).

4. Use domain-specific terms for concepts

    To avoid ambiguities. This is important because we develop software for robotics. Concepts are sometimes named differently across different disciplines and vendors. Use the ones that are most relevant to the context and offer alternatives when more than one apply.

### Styles

This documentation follows a certain format. Although not documented (yet), you are recommended to look at existing documentations and follow closely to offer reader a clear and unified experience.
