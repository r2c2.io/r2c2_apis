# Navigation APIs

## Table of contents

- [Navigation APIs](#navigation-apis)
  - [Table of contents](#table-of-contents)
  - [`/navigate/waypoints/list` (HTTP)](#navigatewaypointslist-http)
  - [`/navigate/to/pose`](#navigatetopose)
  - [`/state/navigation`](#statenavigation)

## `/navigate/waypoints/list` (HTTP)

**Desc:** Return a list of available waypoint ID

**Request Schema:** `null`

**Response Schema:**

```javascript
{
  "id": String,
  "position": {
    "x": Number,
    "y": Number,
    "z": Number,
    "yaw": Number,
  }
}
```

## `/navigate/to/pose`

**Desc:** Global pose to navigate to

**Schema:** 

```javascript
{
  // x, y, z: global coordinate of robot in 3D space, mesaured in meters.
  x: Number,
  y: Number,
  z: Number,

  // yaw: rotation of robot around its vertical axis measured in radian
  yaw: Number,
}
```

**Note:** Pre-defined waypoints contain poses that can be used directly for this topic

## `/state/navigation`

**Desc:** Gives current state of navigation

**Schema:**

```javascript
{
  "state": "TARGET_ARRIVED" | "FAILED" | "NAVIGATING_TO_TARGET",
  "target_pose": { // target pose as specified in /navigate/to/pose
    // x, y, z: global coordinate of robot in a 3D space, mesaured in meters.
    x: Number,
    y: Number,
    z: Number,

    // yaw: rotation of robot around its vertical axis measured in radian
    yaw: Number,
  },
  "current_pose": { // current robot pose
    // x, y, z: global coordinate of robot in 3D space, mesaured in meters.
    x: Number,
    y: Number,
    z: Number,

    // yaw: rotation of robot around its vertical axis measured in radian
    yaw: Number,
  },
}
```
