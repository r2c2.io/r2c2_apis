# Robot Teleoperation API

## Table of Content
- [Robot Teleoperation API](#robot-teleoperation-api)
  - [Table of Content](#table-of-content)
  - [Operations](#operations)
    - [`/perform/movement`](#performmovement)
    - [`/config/motor`](#configmotor)
  - [States](#states)
    - [`/connected`](#connected)
    - [`/state/motor`](#statemotor)


## Operations

### `/perform/movement`

**Desc:** Move the robot. In this case the robot is treated as a single point.

**Schema:** 

```javascript
{
    // x, y, z: linear components of robot's velocity in the 3D space, mesaured in m/s.
    x: Number, // speed along the longitudinal ("forward-backward") axis of the robot. Positive value moves the robot forward.
    y: Number, // speed along cross ("left-right") axis of the robot. Positive value moves the robot to the right
    z: Number, // vertical ("up-down") axis of the robot. Positive value moves the robot up.

    // yaw: rotational speed of around the vertical axis of the robot, measured in rad/s
    yaw: Number,
}
```

**Expected values:** Any natural number for each of x, y, z, and yaw.

**Notes:** Command sent to this topic should be in 100ms interval for smooth control experience.

### `/config/motor`

**Desc:** Turn motor power on or off

**Schema:** `String`

**Expected values:** `"POWER_ON"`, `"POWER_OFF"`.

## States

### `/connected`

**Desc:** Topic that indicates whether the ARC / Jepack is connected to its host robot.

**Schema:** `String`

**Expected value:** `"ROBOT_CONNECTED"` if the robot is connected; `"ROBOT_CONNECTING"` if Jetpack is connecting to the robot; `"ROBOT_DISCONNECTED"` when jetpack is disconnected from the robot.

### `/state/motor`

_Note: This is different from `/config/motor`, where the expected values are `"POWER_ON"` and `"POWER_OFF"`._

**Desc:** Motor state

**Schema:** `String`

**Expected values:** `"POWERED_OFF"`, `"POWERING_ON"`, `"POWERED_ON"`, `"POWERING_OFF"`, `"ERROR"`, `"MOTOR_UNKNOWN"`
