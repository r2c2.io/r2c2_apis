# Sending HTTP Requests

Currently all HTTP requests should be sent with the `POST` method to this one endpoint:

**URL:** `http://ARC_ADDRESS:3000/requests`

**Schema:** 

```javascript
// MIME type: "text/json" or "application/json"
{
    "topic": String,
    ..., // other fields as expected from a HTTP topic, if there's none, they are simply not added
}
```
